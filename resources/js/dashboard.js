window.addEventListener('load', () => {
  const dashboard = document.querySelector('.dashboard');

  dashboard.querySelector('.dashboard__close-aside').addEventListener('click', () => {
    dashboard.classList.toggle('dashboard--aside-toggle');
  });

  dashboard.querySelector('.dashboard__open-aside').addEventListener('click', () => {
    dashboard.classList.toggle('dashboard--aside-toggle');
  });
});