import Vue from 'vue';
import Feed from './components/Feed';

const app = new Vue({
  el: '#app',

  components: { Feed }
});