import Vue from 'vue';
import EditFeed from './components/EditFeed';

const app = new Vue({
  el: '#app',

  components: { EditFeed }
});