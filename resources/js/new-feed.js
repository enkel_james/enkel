import Vue from 'vue';
import NewFeed from './components/NewFeed';

const app = new Vue({
  el: '#app',

  components: { NewFeed }
});