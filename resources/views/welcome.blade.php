@extends('layouts.app')

@section('content')
    <section class="min-h-75 flex flex-col justify-center items-center px-6">
        <h1>Enkel</h1>
        <h3 class="font-serif mb-6">The simple RSS reader</h3>

        <p class="mb-8 text-center">RSS readers are either ugly or clunky. Enkel is clean and simple, leaving you with the content you care about.</p>

        <a href="{{ route('create-feed') }}" class="button button--primary">Get Started</a>
    </section>

	<section class="container mx-auto px-4 py-16 md:p-8">
		<div class="border rounded-lg">
			<div class="border-b flex justify-between items-center p-3">
				<div class="hidden ml-2 mt-1 md:block">
					<span class="bg-grey-light inline-block rounded-full mr-1 h-4 w-4"></span>
					<span class="bg-grey-light inline-block rounded-full mr-1 h-4 w-4"></span>
					<span class="bg-grey-light inline-block rounded-full mr-1 h-4 w-4"></span>
				</div>

				<div class="border flex-grow rounded-lg p-3 text-sm text-grey-light md:rounded md:mx-6">
					https://enkel.fyi/f/example
				</div>
			</div>

			<div class="max-h-75 overflow-hidden">
				<feed class="px-4 py-12 md:px-8" limit="6" feed-data="{{ json_encode($example) }}"></feed>
			</div>
		</div>
	</section>

	<section class="container mx-auto mt-16 px-8 py-16">
		<h2 class="text-center text-4xl mb-16">Account Tiers</h2>

		<div class="max-w-lg mb-16 mx-auto text-center">
			<p>You can get started without an account, but we recommend registering to get the most from Enkel. Better yet, upgrade to our Premium account when it's released for an even better experience.</p>
			<p>Don't worry, we take privacy seriously at Enkel. We don't believe in selling your data, and we hate ads! If you like what we do, we hope you'll upgrade to Premium to keep things running!</p>
		</div>

		<div class="justify-between md:flex relative">
			<div class="border relative p-8 md:w-1/3">
				<h3 class="font-serif mb-8">No Account</h3>

				<ul class="list-reset mb-8 spaced-list">
					<li>Create public feeds.</li>
					<li>Add up to three sources per feed.</li>
				</ul>

				<a class="button md:mb-8 md:absolute md:pin-b" href="{{ route('create-feed') }}">Get started</a>
			</div>

			<div class="bg-grey-light relative p-8 md:w-1/3">
				<h3 class="font-serif mb-8">Registered Users</h3>

				<ul class="list-reset mb-8 spaced-list">
					<li>Create public and private feeds.</li>
					<li>Add as many sources as you like per feed.</li>
					<li>Dashboard to manage and view all your feeds.</li>
				</ul>

				<a class=" button button--primary md:mb-8 md:absolute md:pin-b" href="{{ route('register') }}">Register</a>
			</div>

			<div class="bg-black text-white p-8 md:w-1/3">
				<h3 class="font-serif mb-8 ">Premium Users</h3>

				<ul class="list-reset mb-8 spaced-list">
					<li>Create public and private feeds.</li>
					<li>Add as many sources as you like per feed.</li>
					<li>Dashboard to manage and view all your feeds.</li>
					<li>Dark mode.</li>
					<li>Enhanced content support.</li>
					<li>More features to be confirmed.</li>
				</ul>

				<h4 class="font-serif text-grey-light">Coming soon</h4>
			</div>
		</div>
	</section>

	<footer class="border-t mt-16 px-8 py-4 text-sm font-serif">
		<div class="container flex justify-between mx-auto">
			<div>
				&copy; Enkel {{ date('Y')  }} | <a href="/privacy">Privacy Policy</a>
			</div>

			<div>
				Built with &hearts; by <a href="https://twitter.com/jam3sn_">Jam3sn</a>
			</div>
		</div>
	</footer>

	@section('scripts')
		<script src="{{ mix('js/feed.js') }}" defer></script>
	@endsection
@endsection
