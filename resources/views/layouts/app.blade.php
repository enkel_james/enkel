<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts.head')

<body>
    <div id="app">
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    
        <header class="border-b flex justify-between items-center px-6">
            <a class="py-4" href="{{ url('/') }}">
                <h1 class="mb-0 text-3xl">Enkel</h1>
            </a>

            <nav>
                <a class="ml-6" href="{{ route('create-feed') }}">Create</a>
                @auth
                    <a class="ml-6" href="{{ url('/') }}">My Feeds</a>

                    <a class="ml-6" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                         {{ __('Logout') }}
                    </a>
                @else
                    <a class="ml-6" href="{{ route('login') }}">Login</a>

                    <a class="ml-6" href="{{ route('register') }}">Register</a>
                @endauth
            </nav>
        </header>

        <main>
            @yield('content')
        </main>
    </div>

	<!-- Scripts -->
	<script src="{{ mix('js/app.js') }}" defer></script>
	@yield('scripts')

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129614080-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-129614080-1');
	</script>
</body>
</html>
