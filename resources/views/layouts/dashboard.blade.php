<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts.head')

<body>
    <div id="app" class="dashboard">
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>

        <aside class="dashboard__aside">
            <svg class="dashboard__close-aside" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M278.6 256l68.2-68.2c6.2-6.2 6.2-16.4 0-22.6-6.2-6.2-16.4-6.2-22.6 0L256 233.4l-68.2-68.2c-6.2-6.2-16.4-6.2-22.6 0-3.1 3.1-4.7 7.2-4.7 11.3 0 4.1 1.6 8.2 4.7 11.3l68.2 68.2-68.2 68.2c-3.1 3.1-4.7 7.2-4.7 11.3 0 4.1 1.6 8.2 4.7 11.3 6.2 6.2 16.4 6.2 22.6 0l68.2-68.2 68.2 68.2c6.2 6.2 16.4 6.2 22.6 0 6.2-6.2 6.2-16.4 0-22.6L278.6 256z"/></svg>

            <a class="block mb-12" href="{{ route('home') }}">
                <h1 class="mb-0 text-3xl">Enkel</h1>
            </a>

            <h3 class="font-serif mb-6">Your Feeds</h3>

            <ul class="dashboard__feeds">
                @foreach ($feeds as $feed)
                    <li>
                        <a href="/f/{{ $feed->slug }}" class="{{ (isset($data) && $data['slug'] == $feed->slug) ? 'underline' : '' }}">{{ $feed->name }}</a>
                    </li>
                @endforeach
            </ul>

            <div class="absolute pin-b pin-x p-6 text-center">
				<a class="button block mb-6" href="{{ route('create-feed') }}">New feed</a>

				<a class="button block mb-6" href="{{ route('logout') }}"
				   onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
					{{ __('Logout') }}
				</a>

				<div class="font-serif">
					<a href="mailto:james@enkel.fyi">Support</a>
					<span class="mx-4">|</span>
					<a href="/privacy">Privacy Policy</a>
				</div>
            </div>
        </aside>

        <main class="dashboard__content">
            <svg class="dashboard__open-aside" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M432 176H80c-8.8 0-16-7.2-16-16s7.2-16 16-16h352c8.8 0 16 7.2 16 16s-7.2 16-16 16zM432 272H80c-8.8 0-16-7.2-16-16s7.2-16 16-16h352c8.8 0 16 7.2 16 16s-7.2 16-16 16zM432 368H80c-8.8 0-16-7.2-16-16s7.2-16 16-16h352c8.8 0 16 7.2 16 16s-7.2 16-16 16z"/></svg>

            @yield('content')
        </main>
    </div>

    <!-- Scripts -->
	<script src="{{ mix('js/app.js') }}" defer></script>
	<script src="{{ mix('js/dashboard.js') }}" defer></script>
	@yield('scripts')

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129614080-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-129614080-1');
	</script>
</body>
</html>
