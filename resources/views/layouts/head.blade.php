<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#000000">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="theme-color" content="#ffffff">

  <title>Enkel</title>
  <meta name="Description" content="The simple RSS reader">

  <meta property="og:title" content="Enkel">
  <meta property="og:site_name" content="Enkel.">
  <meta property="og:description" content="The simple RSS reader">
  <meta property="og:image" content="https://enkel.fyi/enkel-card.jpg">
  <meta property="og:url" content="https://enkel.fyi">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@jam3sn_">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
