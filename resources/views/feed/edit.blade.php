@extends('layouts.dashboard')

@section('content')
    <section class="container mx-auto max-w-md px-8 py-16">
        <h1>Update your feed</h1>

        <edit-feed feed-data="{{ isset($feed) ? json_encode($feed) : '' }}"></edit-feed>
    </section>

    @section('scripts')
        <script src="{{ mix('js/edit-feed.js') }}" defer></script>
	@endsection
@endsection


