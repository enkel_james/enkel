@extends(Auth::check() ? 'layouts.dashboard' : 'layouts.app')

@section('content')
    <section class="container mx-auto px-8 py-16">
        <feed auth="{{ Auth::check() }}" feed-data="{{ json_encode($data) }}"></feed>
    </section>

    @section('scripts')
        <script src="{{ mix('js/feed.js') }}" defer></script>
    @endsection
@endsection


