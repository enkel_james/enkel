@extends(Auth::check() ? 'layouts.dashboard' : 'layouts.app')

@section('content')
    <section class="container mx-auto max-w-md px-8 py-16">
        <h1>Create a new feed</h1>
        <p class="mb-12">Just enter a valid url to an RSS feed to get started.</p>

        <new-feed auth="{{ Auth::check() ? true : false }}"></new-feed>
    </section>

    @section('scripts')
        <script src="{{ mix('js/new-feed.js') }}" defer></script>
    @endsection
@endsection


