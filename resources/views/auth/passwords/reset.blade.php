@extends('layouts.app')

@section('content')
<section class="container mx-auto max-w-md px-8 py-16">
    <h1 class="{{ session('status') ? '' : 'mb-12' }}">Update Password</h1>

    <form method="POST" action="{{ route('password.update') }}">
        @csrf

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="input-group">
            <label>Email</label>

            @if ($errors->has('email'))
                <p class="input-error italic mb-4">{{ $errors->first('email') }}</p>
            @endif

            <input id="email" type="email" class="mb-4" name="email" value="{{ $email ?? old('email') }}" required autofocus>
        </div>

        <div class="input-group">
            <label>Password</label>

            @if ($errors->has('password'))
                <p class="input-error italic mb-4">{{ $errors->first('password') }}</p>
            @endif

            <input id="password" type="password" class="mb-4" name="password" required>
        </div>

        <div class="input-group">
            <label>Confirm Password</label>

            @if ($errors->has('password'))
                <p class="input-error italic mb-4">{{ $errors->first('password') }}</p>
            @endif

            <input id="password-confirm" type="password" class="mb-4" name="password_confirmation" required>
        </div>

        <button type="submit" class="button button-primary float-right">
            {{ __('Reset Password') }}
        </button>
    </form>
</section>
@endsection
