@extends('layouts.app')

@section('content')
<section class="container mx-auto max-w-md px-8 py-16">
    <h1 class="{{ session('status') ? '' : 'mb-12' }}">Reset Password</h1>

    @if (session('status'))
        <p>{{ session('status') }}</p>
    @else
        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="input-group">
                <label>Email</label>

                @if ($errors->has('email'))
                    <p class="input-error italic mb-4">{{ $errors->first('email') }}</p>
                @endif

                <input id="email" type="email" class="mb-4" name="email" value="{{ old('email') }}" required autofocus>
            </div>

            <button type="submit" class="button button--primary float-right">
                {{ __('Send Reset Link') }}
            </button>
        </form>
    @endif
</section>
@endsection
