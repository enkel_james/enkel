@extends('layouts.app')

@section('content')
<section class="container mx-auto max-w-md px-8 py-16">
    <h1>Register</h1>

    <p class="mb-12">Welcome to Enkel! Let's get started.</p>

    <form method="POST" action="{{ route('register') }}">
        @csrf

        <div class="input-group">
            <label>Name</label>

            @if ($errors->has('name'))
                <p class="input-error italic mb-4">{{ $errors->first('name') }}</p>
            @endif

            <input id="name" type="text" class="mb-4" name="name" value="{{ old('name') }}" required autofocus>
        </div>

        <div class="input-group">
            <label>Email</label>

            @if ($errors->has('email'))
                <p class="input-error italic mb-4">{{ $errors->first('email') }}</p>
            @endif

            <input id="email" type="email" class="mb-4" name="email" value="{{ old('email') }}" required>
        </div>

        <div class="input-group">
            <label>Password</label>

            @if ($errors->has('password'))
                <p class="input-error italic mb-4">{{ $errors->first('password') }}</p>
            @endif

            <input id="password" type="password" class="mb-4" name="password" required>
        </div>

        <div class="input-group">
            <label>Confirm Password</label>

            <input id="password-confirm" type="password" class="mb-4" name="password_confirmation" required>
        </div>

		<div class="input-group">
			<p>By clicking Register, you agree to our <a class="underline" href="/privacy">Privacy Policy</a>. You can learn how we collect and use your data in our <a class="underline" href="/privacy">Privacy Policy</a></p>
		</div>

        <button type="submit" class="button button--primary float-right">
            {{ __('Register') }}
        </button>
    </form>
</section>
@endsection
