@extends('layouts.app')

@section('content')
<section class="container mx-auto max-w-md px-8 py-16">
    <h1 class="mb-12">Login</h1>

    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="input-group">
            <label>Email</label>

            @if ($errors->has('email'))
                <p class="input-error italic mb-4">{{ $errors->first('email') }}</p>
            @endif

            <input id="email" type="email" class="mb-4" name="email" value="{{ old('email') }}" required autofocus>
        </div>

        <div class="input-group">
            <label>Password</label>

            @if ($errors->has('password'))
                <p class="input-error italic mb-4">{{ $errors->first('password') }}</p>
            @endif

            <input id="password" type="password" class="mb-4" name="password" required>
        </div>


        <div class="flex items-center mb-8">
            <input class="mr-3" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

            <label for="remember">
                {{ __('Remember Me') }}
            </label>
        </div>

        <button type="submit" class="button button--primary float-right">
            {{ __('Login') }}
        </button>

        <a class="button float-right mr-4" href="{{ route('password.request') }}">
            {{ __('Forgot Password?') }}
        </a>
    </form>
</section>
@endsection
