@extends('layouts.app')

@section('content')
    <section class="h-screen flex justify-between items-center m-auto -mt-24 max-w-md px-6">
        <div>
            <h1 class="mb-12">404</h1>

            <p class="mb-12">Whoops, how'd you end up here?</p>

            <a href="{{ route('home') }}" class="button">Home</a>
        </div>
    </section>
@endsection