@extends('layouts.app')

@section('content')
    <section class="h-screen flex justify-between items-center m-auto -mt-24 max-w-md px-6">
        <div>
            <h1>500</h1>

            <p class="mb-12">Whoops, something went wrong. If you can, please submit a report below.</p>

            <div class="border p-6">
                @if(app()->bound('sentry') && !empty(Sentry::getLastEventID()))
                    <div class="subtitle">Error ID: {{ Sentry::getLastEventID() }}</div>

                    <!-- Sentry JS SDK 2.1.+ required -->
                    <script src="https://cdn.ravenjs.com/3.3.0/raven.min.js"></script>

                    <script>
                        Raven.showReportDialog({
                            eventId: '{{ Sentry::getLastEventID() }}',
                            // use the public DSN (dont include your secret!)
                            dsn: 'https://0e6d7d2924124563af04f9d6b1c74922@sentry.io/1300544',
                            user: {
                                'name': 'Jane Doe',
                                'email': 'jane.doe@example.com',
                            }
                        });
                    </script>
                @endif
            </div>
        </div>
    </section>
@endsection