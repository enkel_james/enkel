@extends('layouts.app')

@section('content')
	<section class="container mx-auto px-8 py-16">
		<h1 class="mb-12">Stats</h1>

		<article class="flex items-center p-8 text-center w-full">
			<div class="w-1/3"></div>

			<div class="w-1/3">
				<h2 class="mb-2">Today</h2>
			</div>

			<div class="w-1/3">
				<h2 class="mb-2">Total</h2>
			</div>
		</article>


		<article class="border-t flex items-center p-8 text-center w-full">
			<div class="w-1/3">
				<h2 class="mb-2">Users</h2>
			</div>

			<div class="w-1/3">
				<h3 class="font-serif text-5xl">{{ $stats['createdUsersToday'] }}</h3>
			</div>

			<div class="w-1/3">
				<h3 class="font-serif text-5xl">{{ $stats['totalUsers'] }}</h3>
			</div>
		</article>

		<article class="border-t flex items-center p-8 text-center w-full">
			<div class="w-1/3">
				<h2 class="mb-2">Feeds</h2>
			</div>

			<div class="w-1/3">
				<h3 class="font-serif text-5xl">{{ $stats['createdFeedsToday'] }}</h3>
			</div>

			<div class="w-1/3">
				<h3 class="font-serif text-5xl">{{ $stats['totalFeeds'] }}</h3>
			</div>
		</article>

		<article class="border-t flex items-center p-8 text-center w-full">
			<div class="w-1/3">
				<h2 class="mb-2">Sources</h2>
			</div>

			<div class=" w-1/3">
				<h3 class="font-serif text-5xl">{{ $stats['createdSourcesToday'] }}</h3>
			</div>

			<div class="w-1/3">
				<h3 class="font-serif text-5xl">{{ $stats['totalSources'] }}</h3>
			</div>
		</article>

		<article class="border-t flex items-center p-8 text-center w-full">
			<div class="w-1/3">
				<h2 class="mb-2">Feeds Read</h2>
			</div>

			<div class=" w-1/3">
				<h3 class="font-serif text-5xl">{{ $stats['readFeedsToday'] }}</h3>
			</div>
		</article>
	</section>
@endsection
