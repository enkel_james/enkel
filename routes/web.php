<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Public
|--------------------------------------------------------------------------
*/

Auth::routes();

Route::get('/', function () {
    if (Auth::guest()) {
		$feed = App\Feed::where('slug', 'example')->firstOrFail();
		$feed->increment('hit_count');

		$example = [
			'title' => $feed->name,
			'slug' => $feed->slug,
			'sources' => $feed->sources->toArray(),
			'count' => $feed->hit_count,
			'private' => $feed->private,
			'created' => $feed->created_at->format('Y-m-d H:i:s'),
			'updated' => $feed->updated_at->format('Y-m-d H:i:s'),
		];

        return view('welcome', compact('example'));
    } else {
        return redirect()->route('list-feeds');
    }
})->name('home');

Route::get('privacy', function () {
	return view('privacy');
});

/*
|--------------------------------------------------------------------------
| Feed
|--------------------------------------------------------------------------
*/
Route::get('f', 'FeedController@index')->middleware('auth')->name('list-feeds');

Route::get('f/new', 'FeedController@create')->name('create-feed');
Route::post('f/store', 'FeedController@store');

Route::get('f/{slug}', 'FeedController@show')->where('slug', '[a-zA-Z0-9]+')->name('feed');

Route::get('f/edit/{slug}', 'FeedController@edit')->middleware('auth')->name('edit-feed');
Route::put('f/update/{slug}', 'FeedController@update')->middleware('auth');

Route::post('f/delete/{slug}', 'FeedController@destroy')->middleware('auth');


/*
|--------------------------------------------------------------------------
| Admin
|--------------------------------------------------------------------------
*/
Route::get('admin', 'AdminController@index');


/*
|--------------------------------------------------------------------------
| Dev
|--------------------------------------------------------------------------
*/

if (env('APP_ENV') == 'local') {

    // Render email in browser
    Route::get('/email', function () {
        return new App\Mail\UserWelcome();
    });
}
