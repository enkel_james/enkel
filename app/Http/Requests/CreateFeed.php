<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateFeed extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:1|regex:/^[A-Za-z0-9_~\-!@#\$%\^&\(\)\s]+$/',
            'sources.*.url' => 'url'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'A feed name is required',
            'name.regex' => 'Your name can contain letters, spaces,  numbers and \'_ - ! @ # $ &\'.',
            'sources.*'  => 'A valid URL must be entered e.g. https://example.com/rss.',
        ];
    }
}

