<?php

namespace App\Http\Controllers;

use App\NewsAPI;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class NewsAPIController extends Controller
{
    private $client;

    /**
     * Create Guzzle Client
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://newsapi.org/v2/',
            'headers' => [
                'Authorization' => 'Bearer ' . env('NEWSAPI_TOKEN')
            ]
        ]);
    }

    /**
     * Get News API sources
     *
     * @return json
     */
    public function sources()
    {
        $response = $this->client->request('GET', 'sources');

        if ($response->getStatusCode() !== 200) {
            return response()->json(['API error occurred'], 500);
        }

        return response()->json($response->getBody()->getContents());
    }

     /**
     * Get headlines from source
     *
     * @param string $source
     * @return json
     */
    public function headlines($source)
    {
        $response = $this->client->request('GET', 'top-headlines', [
            'query' => ['sources' => $source]
        ]);

        if ($response->getStatusCode() !== 200) {
            return response()->json(['API error occurred'], 500);
        }

        return response()->json($response->getBody()->getContents());
    }
}
