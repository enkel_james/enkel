<?php

namespace App\Http\Controllers;

use App\Sources;
use Illuminate\Http\Request;

class SourcesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sources  $sources
     * @return \Illuminate\Http\Response
     */
    public function show(Sources $sources)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sources  $sources
     * @return \Illuminate\Http\Response
     */
    public function edit(Sources $sources)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sources  $sources
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sources $sources)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sources  $sources
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sources $sources)
    {
        //
    }
}
