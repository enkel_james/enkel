<?php

namespace App\Http\Controllers;

use App\Feed;
use App\Source;
use App\Http\Requests\CreateFeed;
use Illuminate\Http\Request;

class FeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (isset(\Auth::user()->firstFeed()->slug)) {
            return redirect()->route('feed', \Auth::user()->firstFeed()->slug);
        } else {
            return redirect()->route('create-feed');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Dashboard
        if (\Auth::check()) {
            $feeds = \Auth::user()->feeds();
        }

        return view('feed/create', compact('feeds'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\CreateFeed  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateFeed $request)
    {
        // Create Feed
        $feed = new Feed;

        $feed->name = $request->get('name');
        $feed->slug = uniqid();
        $feed->private = $request->get('private');

        if (\Auth::check()) {
            $feed->user_id = \Auth::user()->id;
        }

        $feed->save();

        // Create sources
        foreach(\request('sources') as $newSource) {
            $source = Source::where('url', $newSource['url'])->first();

            if (! $source) {
                $source = new Source;
                $source->name = $newSource['name'];
                $source->url = $newSource['url'];
                $source->private = $request->get('private');

                $source->save();
            }

            $feed->sources()->attach($source);
        }

        return response()->json(array('slug' => $feed->slug));
    }

    /**
     * Display the specified resource.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $feed = Feed::where('slug', $slug)->firstOrFail();

        // Redirect if user isn't owner and feed it private
        if ((\Auth::guest() || \Auth::user()->id !== $feed->user_id) && $feed->private) {
            return redirect()->route('home');
        }

        $feed->increment('hit_count');
        
        $data = array(
            'title' => $feed->name,
            'slug' => $feed->slug,
            'sources' => $feed->sources->toArray(),
            'count' => $feed->hit_count,
            'private' => $feed->private,
            'created' => $feed->created_at->format('Y-m-d H:i:s'),
            'updated' => $feed->updated_at->format('Y-m-d H:i:s'),
        );

        // Dashboard
        if (\Auth::check()) {
            $feeds = \Auth::user()->feeds();
        }

        return view('feed/feed', compact('data', 'feeds'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        // Dashboard
        $feeds = \Auth::user()->feeds();

        // Get feed
        $feed = Feed::where('slug', $slug)->firstOrFail();

        // Redirect if user isn't owner
        if (\Auth::user()->id !== $feed->user_id) {
            return redirect('/f/' . $slug);
        }

        // Grab the feeds sources
		$feed->sources = $feed->sources->toArray();

        return view('feed/edit', compact('feed', 'feeds'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $slug
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update($slug, Request $request)
    {
        // Get feed
        $feed = Feed::where('slug', $slug)->first();

        // Redirect if user isn't owner
        if (\Auth::user()->id !== $feed->user_id) {
            return redirect('/f/' . $slug);
        }

        $feed->name = $request->get('name');
        $feed->private = $request->get('private');

        $feed->save();

		// Create sources
		$sources = [];
		foreach(\request('sources') as $newSource) {
			$source = Source::where('url', $newSource['url'])->first();

			if (! $source) {
				$source = new Source;
				$source->name = $newSource['name'];
				$source->url = $newSource['url'];
				$source->private = $request->get('private');

				$source->save();
			}

			array_push($sources, $source->id);
		}

		$feed->sources()->sync($sources);

        return response()->json(array('slug' => $feed->slug));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        Feed::where('slug', $slug)->delete();

        return response()->json(array('success' => true));
    }
}
