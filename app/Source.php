<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    /**
     * Source has feeds
     */
    public function feeds()
    {
        return $this->belongsToMany(Feed::class);
    }
}
