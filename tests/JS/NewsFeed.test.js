import { mount } from '@vue/test-utils';
import NewsFeed from '../../resources/js/components/NewFeed.vue';

describe('NewsFeed', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(Component)
    expect(wrapper.isVueInstance()).toBeTruthy()
  });

  test('renders correctly', () => {
    const wrapper = mount(Component)
    expect(wrapper.element).toMatchSnapshot()
  })
})